CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration


INTRODUCTION
------------

This is a README file for "Locale Regular Expression Search" module.

This module allows admin users to search interface text translations with
regular expression.


REQUIREMENTS
------------

This module alters the page provided by Locale module in core and needs Locale
module to be enabled.

Also, this module requires that the database used for the Drupal instance
supports `REGEXP` operator in its `WHERE` query.


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7 for further
information.

Navigate to administer >> modules. Enable Locale Regular Expression Search
module.

If the module is installed, the page to search interface text translations
(admin/config/regional/translate/translate) is automatically changed. You can
use regular expression patterns in the string field.


CONFIGURATION
-------------

There's no configuration option for this module at the moment.
